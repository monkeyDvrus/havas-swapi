import '../styles/Pagination.sass';
import React, {useState, useEffect, useCallback} from 'react';
import PropTypes from 'prop-types';

const LEFT_PAGE = 'LEFT';
const RIGHT_PAGE = 'RIGHT';

/** Créer une plage de nombres */
const range = (from, to, step = 1) => {
  let i = from;
  const range = [];

  while (i <= to) {
    range.push(i);
    i += step;
  }

  return range;
};

function Pagination ({totalRecords, pageLimit, pageNeighbours, onPageChange}) {
  const totalPages = Math.ceil(totalRecords / pageLimit);

  const [currentPage, setCurrentPage] = useState(1);

  /** Modifie l'état de la page courante et fait remonter les données de la nouvelle pagination */
  // Utilisation de useCallback car goToPage est appelé dans useEffect
  const goToPage = useCallback((page) => {
    const currentPage = Math.max(0, Math.min(page, totalPages));

    const paginationData = {
      currentPage,
      totalPages: totalPages,
      pageLimit: pageLimit,
      totalRecords: totalRecords
    };

    setCurrentPage(currentPage);
    onPageChange(paginationData);
  }, [onPageChange, pageLimit, totalPages, totalRecords]);

  function handleClick (page) {
    goToPage(page);
  };

  function handleMoveLeft () {
    goToPage(currentPage - pageNeighbours * 2 - 1);
  };

  function handleMoveRight () {
    goToPage(currentPage + pageNeighbours * 2 + 1);
  };

  /** Gère la logique de base pour générer les numéros de page à afficher  */
  function fetchPageNumbers () {
    const totalPagesFetch = totalPages;
    const currentPageFetch = currentPage;
    const pageNeighboursFetch = pageNeighbours;

    /** Le nombre total de pages qui seront affichées sur le contrôle. */
    const totalNumbers = pageNeighboursFetch * 2 + 3;
    /** Le nombre total de pages à afficher, plus deux blocs supplémentaires
     * pour les indicateurs de page gauche et droite. */
    const totalBlocks = totalNumbers + 2;

    if (totalPagesFetch > totalBlocks) {
      let pages = [];

      const leftBound = currentPageFetch - pageNeighbours;
      const rightBound = currentPageFetch + pageNeighbours;
      const beforeLastPage = totalPagesFetch - 1;

      const startPage = leftBound > 2 ? leftBound : 2;
      const endPage = rightBound < beforeLastPage ? rightBound : beforeLastPage;

      pages = range(startPage, endPage);

      const pagesCount = pages.length;
      const singleSpillOffset = totalNumbers - pagesCount - 1;

      const leftSpill = startPage > 2;
      const rightSpill = endPage < beforeLastPage;

      const leftSpillPage = LEFT_PAGE;
      const rightSpillPage = RIGHT_PAGE;

      if (leftSpill && !rightSpill) {
        const extraPages = range(startPage - singleSpillOffset, startPage - 1);
        pages = [leftSpillPage, ...extraPages, ...pages];
      } else if (!leftSpill && rightSpill) {
        const extraPages = range(endPage + 1, endPage + singleSpillOffset);
        pages = [...pages, ...extraPages, rightSpillPage];
      } else if (leftSpill && rightSpill) {
        pages = [leftSpillPage, ...pages, rightSpillPage];
      }
      return [1, ...pages, totalPagesFetch];
    }
    return range(1, totalPagesFetch);
  };

  useEffect(() => {
    goToPage(1);
  }, [totalRecords, goToPage]);

  const pages = fetchPageNumbers();
  return (
    <div className="App-pagination">
        <nav aria-label="Characters Pagination">
          <ul className="pagination">
            {pages.map((page, index) => {
              if (page === LEFT_PAGE)
                return (
                  <li key={index} className="page-item">
                    <button
                      className="page-link prev"
                      aria-label="Previous"
                      onClick={handleMoveLeft}
                    >
                      <span aria-hidden="true">&lt;</span>
                    </button>
                  </li>
                );

              if (page === RIGHT_PAGE)
                return (
                  <li key={index} className="page-item">
                    <button
                      className="page-link next"
                      aria-label="Next"
                      onClick={handleMoveRight}
                    >
                      <span aria-hidden="true">&gt;</span>
                    </button>
                  </li>
                );

              return (
                <li
                  key={index}
                  className={`page-item${
                    currentPage === page ? " active" : ""
                  }`}
                >
                  <button
                    className="page-link"
                    onClick={() => handleClick(page)}
                  >
                    {page}
                  </button>
                </li>
              );
            })}
          </ul>
        </nav>
    </div>
  );
};

Pagination.defaultProps = {
  pageLimit: 6,
  pageNeighbours: 0
}

Pagination.propTypes = {
  totalRecords: PropTypes.number.isRequired,
  pageLimit: PropTypes.number.isRequired,
  pageNeighbours: PropTypes.number.isRequired,
  onPageChange: PropTypes.func.isRequired
}

export default Pagination;