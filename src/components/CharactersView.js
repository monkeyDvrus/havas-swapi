import '../styles/CharactersView.sass';
import PropTypes from 'prop-types';

function CharactersView ({listCharacters, getSpecie, getHomeworld}) {

  return (
    <div className="Characters-view">
      {listCharacters.length === 0 ?
      (
        <div className="Characters-view__list">
          <p>No result</p>
        </div>
      ) : (
        <ul className="Characters-view__list">
          {listCharacters.map(person => (
            <li key={person.url} className={`Characters-view__item ${person.name}`}>
              <div className="Characters-view__item--name">
                <h2>{person.name}</h2>
              </div>
              <div className="Characters-view__item--infos">
                <dl>
                  <dt>Species</dt>
                  <dd>{person.species.length === 0 ? "n/a" : person.species.map(specie => getSpecie(specie))}</dd>
                  <dt>Gender</dt>
                  <dd>{person.gender}</dd>
                  <dt>Birth</dt>
                  <dd>{person.birth_year}</dd>
                  <dt>Planet</dt>
                  <dd>{person.homeworld === null ? "n/a" : getHomeworld(person.homeworld)}</dd>
                </dl>
              </div>
            </li>
          ))}
        </ul>
      )}
    </div>
  );
};

CharactersView.propTypes = {
  listCharacters: PropTypes.array.isRequired,
  getSpecie: PropTypes.func.isRequired,
  getHomeworld: PropTypes.func.isRequired
};

export default CharactersView;