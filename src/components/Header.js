import '../styles/Header.sass';
import { ReactComponent as Logo } from '../assets/swAPI.svg';
import {useState} from 'react';
import PropTypes from 'prop-types';

function Header ({
    filmsList,
    speciesList,
    onNameChange,
    searchName,
    onFilmChange,
    searchFilm,
    onSpecieChange,
    searchSpecie,
    onReset,
    counterResult
  }) {
  const [menuIsOpen, setMenuIsOpen] = useState(false);
  const [mainMenu, setMainMenu] = useState("");

  function handleClickMenuButton () {
    menuIsOpen
      ? closeMenu()
      : openMenu();
  }

  function openMenu () {
    setMenuIsOpen(true);
    setMainMenu('open');
  }

  function closeMenu () {
    setMenuIsOpen(false);
    setMainMenu("close");
    setTimeout(() => {
      setMainMenu("");
    }, 500);
  }

  return (
    <header className="App-header">
      <div className="App-header__menu-bar">
        <a href="/" className="App-header__logo">
            <Logo />
        </a>
        <button
          className="App-header__hamburger"
          onClick={handleClickMenuButton}
          type="button"
          name="Menu hamburger">
          <div
            className={`menu-toggle ${menuIsOpen ? "open" : "close"}`}
            aria-expanded={menuIsOpen}
          >
          </div>
        </button>
        <div className={`App-header__menu-filter ${mainMenu}`}>
          <input
            value={searchName}
            onChange={(e) => onNameChange(e)}
            type="search"
            placeholder="Search by name"
          />
          <div className="select-bar">
            <select 
              value={searchFilm}
              onChange={(e) => onFilmChange(e)}
              name="films" 
              id="films"
            >
              <option value="">Filter by movies</option>
              {filmsList.map(film => (
                <option key={film.url} value={film.url}>{film.title}</option>
              ))}
            </select>
            <select
              value={searchSpecie}
              onChange={onSpecieChange}
              name="species"
              id="species"
            >
              <option value="">Filter by species</option>
              <option value="n/a">n/a</option>
              {speciesList.map(specie => (
                <option key={specie.url} value={specie.url}>{specie.name}</option>
              ))}
            </select>
          </div>
          <div className="button-bar">
            <button
              onClick={onReset}
              className="reset"
            >Reset filters</button>
            <button
              onClick={closeMenu}
              className="apply"
            >Apply - {counterResult} result{counterResult > 1 ? "s" : ""}</button>
          </div>
        </div>
      </div>
    </header>
  );
};

Header.propTypes = {
  filmsList: PropTypes.array.isRequired,
  speciesList: PropTypes.array.isRequired,
  onNameChange: PropTypes.func.isRequired,
  searchName: PropTypes.string.isRequired,
  onReset: PropTypes.func.isRequired,
  counterResult: PropTypes.number.isRequired
}

export default Header;