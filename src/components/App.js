import '../styles/App.sass';
import CharactersView from './CharactersView';
import Header from './Header';
import Pagination from './Pagination';

import { useState, useEffect, useCallback } from 'react';
import axios from 'axios';

function App() {
  const [error, setError] = useState(null);
  const [isLoaded, setIsLoaded] = useState(false);
  const [items, setItems] = useState([]);
  const [films, setFilms] = useState([]);
  const [species, setSpecies] = useState([]);
  const [planets, setPlanets] = useState([]);
  const [searchName, setSearchName] = useState('');
  const [searchFilm, setSearchFilm] = useState('');
  const [searchSpecies, setSearchSpecies] = useState('');
  const [currentCharacters, setCurrentCharacters] = useState([]);

  function handleSearchName (e) {
    setSearchName(e.target.value);
  }

  function handleSearchFilm (e) {
    setSearchFilm(e.target.value);
  }

  function handleSearchSpecie (e) {
    setSearchSpecies(e.target.value);
  }

  function handleReset () {
    setSearchName("");
    setSearchFilm("");
    setSearchSpecies("");
  }

  function getSpecie (url) {
    return species.find(specie => specie.url === url).name;
  }

  function getHomeworld (url) {
    return planets.find(planet => planet.url === url).name;
  }

  /** Filtres la liste des personnages */
  const dynamicSearch = useCallback(() => {
    let filter = items;
    if (searchName.length !== 0 || searchFilm.length !== 0 || searchSpecies.length !== 0) {
      if (searchName.length !== 0) filter = filter.filter(person => person.name.toLowerCase().includes(searchName.toLowerCase()));
      if (searchFilm.length !== 0) filter = filter.filter(person => person.films.includes(searchFilm));
      if (searchSpecies.length !== 0) {
        searchSpecies === "n/a"
          ? filter = filter.filter(person => person.species.length === 0)
          : filter = filter.filter(person => person.species.includes(searchSpecies));
      };
    };
    return filter;
  }, [searchName, searchFilm, searchSpecies, items])

  /** Permet à Pagination de faire remonter les informations pour la mise à jour
   * des personnages visible */
  const onPageChange = useCallback ((data) => {
    const filteredCharacters = dynamicSearch();
    const {currentPage, pageLimit} = data;
    const offset = (currentPage - 1) * pageLimit;
    const currentCharacters = filteredCharacters.slice(offset, offset + pageLimit);
    setCurrentCharacters(currentCharacters);
  }, [dynamicSearch])

  useEffect(() => {
    const getPeople = (url, dataListPrev, setData) => {
      axios.get(url)
        .then(res => {
          const result = res.data;
          const dataList = [...dataListPrev, ...result.results];
          if (result.next !== null) {
            getPeople(result.next, dataList, setData)
          } else {
            setIsLoaded(true);
            setData(dataList);
          };
        })
        .catch(error => {
          setError(error);
        });
    };
    const getData = (url, dataListPrev, setData) => {
      axios.get(url)
        .then((res) => {
          const result = res.data;
          const dataList = [...dataListPrev, ...result.results];
          if(result.next !== null) {
            getData(result.next, dataList, setData);
          } else {
            setData(dataList);
          };
        })
        .catch(error => {
          setError(error);
        });
    };

    getPeople('https://swapi.dev/api/people/', [], setItems);
    getData('https://swapi.dev/api/films/', [], setFilms);
    getData('https://swapi.dev/api/species/', [], setSpecies);
    getData('https://swapi.dev/api/planets/', [], setPlanets);
  }, [])

  return (
    <div className="App">
      <Header
        filmsList={films}
        speciesList={species}
        searchName={searchName}
        onNameChange={handleSearchName}
        searchFilm={searchFilm}
        onFilmChange={handleSearchFilm}
        searchSpecie={searchSpecies}
        onSpecieChange={handleSearchSpecie}
        onReset={handleReset}
        counterResult={dynamicSearch().length}
      />
      <main>
        <div className="App-home">
          <h1>Discover the star wars characters through the <strong>swAPI API.</strong></h1>
          <q>May the force be with you.</q>
          <a href="https://swapi.dev/" target="_blank" rel="noopener noreferrer">See API's website</a>
        </div>
        {isLoaded ? (
          error === null ? (
            <div className="App-view">
              <CharactersView
                listCharacters={currentCharacters}
                getSpecie={getSpecie}
                getHomeworld={getHomeworld}
              />
              <Pagination
                totalRecords={dynamicSearch().length}
                onPageChange={onPageChange}
              />
            </div>
          ) : (
            <div className="App-view">
              <p>An error occurred while downloading the data</p>
            </div>
          )
        ) : (
          <div className="App-view">
            <div className="App-view__spinner">
              <div className="App-view__bounce App-view__bounce--empire1"></div>
              <div className="App-view__bounce App-view__bounce--empire2"></div>
              <div className="App-view__bounce App-view__bounce--empire3"></div>
            </div>
          </div>
        )}
      </main>
    </div>
  );
}

export default App;
